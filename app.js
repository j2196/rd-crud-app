const express = require("express");
const app = express();
const cors = require("cors");
const { port} = require("./configurations");

//Routes
const getRoute = require("./Routes/getRoutes");
const postRoute = require("./Routes/postRoutes");
const putRoute = require("./Routes/putRoute");

//Parsing the body data to json
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//Enabling cors
app.use(cors());

app.set('view engine', 'ejs')

//CRUD Routes
app.use("/", [getRoute, postRoute, putRoute]);


app.listen(port, () => {
  console.log(`Server listening on port ${port}...`);
});
