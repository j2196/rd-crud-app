const express = require("express");
const router = express.Router();

const {editUsers} = require("../controllers/putUsersControllers")

//Put Route
router.post("/edit", editUsers);


module.exports = router;