const express = require("express");
const router = express.Router();

const {getUsersController, verifyAccount, editUserForm, deleteUserForm, createUserForm} = require("../controllers/getUsersControllers")


//Get Route
router.get("/", getUsersController);

router.get("/createUser", createUserForm)

router.get("/verifyAccount/:id", verifyAccount);

router.get("/editUser/:id", editUserForm);

router.get('/deleteUser/:id', deleteUserForm)

module.exports = router;
