const express = require("express");
const router = express.Router();

const {createUser} = require("../controllers/postUsersControllers");
const {userDataFromDb} =  require("../controllers/downloadDbDataController")

//Post Request
router.post("/create", createUser);
router.post("/downloadUserData", userDataFromDb)

module.exports = router;