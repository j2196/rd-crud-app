const MongoClient = require("mongodb").MongoClient;
const {url, dbName} = require('../configurations');

let dbInstance;

module.exports = {
  initialDbConnection: (callbackFunction) => {
    MongoClient.connect(url, { useNewUrlParser: true }, (error, client) => {
      dbInstance = client.db(dbName);
      console.log(`connected to database - ${url}`);
      return callbackFunction(error, dbInstance)
    })
  }
}





