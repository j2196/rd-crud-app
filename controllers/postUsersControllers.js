const collectionName = "usersData";
const {userName, userPassword } = require("../configurations")
const nodemailer = require("nodemailer");

const connectionObject = require("../Db/connection");
let db;

connectionObject.initialDbConnection((error, dbInstance) => {
  if(error) {
    console.log(error);
    throw error;
  } else {
    db = dbInstance;
  }
})

//function to send mail after a user is added to the db
function sendMailToUser(receiverEmailAddress, idOfCreatedUser) {
    //Senders Information
    const transporter = nodemailer.createTransport({
      service: "hotmail",
      auth: {
        user: userName,
        pass: userPassword,
      },
    });
  
    //Receivers Info and mail body
    const options = {
      from: userName,
      to: `${receiverEmailAddress}`,
      subject: "Activate account",
      html: `<a href=http://localhost:4000/verifyAccount/${idOfCreatedUser} target=_blank>Click here to Activate Your Account</a>`,
    };
  
    //Sending Email
    transporter.sendMail(options, (error, info) => {
      if (error) {
        console.log(error);
      } else {
        console.log("Email Sent: ", info.response);
      }
    });
  }

const createUser = (req, res) => {
    if (req.body.name && req.body.email) {
      db.collection(collectionName)
        .insertOne({...req.body, status: 'inActive', isDeleted: false})
        .then((data) => {
          //call function and sending the email address to it
          sendMailToUser(req.body.email, data.insertedId.toString())
        })
        .then(() => {
          res.redirect("/")
        })
        .catch((error) => {
          console.log(error);
          res
            .status(500)
            .json({ success: false, Message: "Database connection failure" });
        });
    } else {
      res
        .status(400)
        .json({ success: false, Message: "Please Enter name and email address" });
    }
}


module.exports = {
    createUser,
}