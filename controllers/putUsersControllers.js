const collectionName = "usersData";
const connectionObject = require("../Db/connection");

let db;

connectionObject.initialDbConnection((error, dbInstance) => {
  if(error) {
    console.log(error);
    throw error;
  } else {
    db = dbInstance;
  }
})

const editUsers = (req, res) => {
    db.collection(collectionName)
      .find()
      .toArray()
      .then((data) => {
        data.map((userObject) => {
          if (req.body.id === userObject._id.toString()) {
            db.collection(collectionName).updateMany(
              { _id: userObject._id },
              {
                $set: {
                  name: req.body.name,
                  email: req.body.email,
                },
              }
            );
            return userObject;
          } else {
            return userObject;
          }
        });
      })
      .then(() => {
        res.redirect('/')
      })
      .catch((error) => {
        console.log(error);
        res
          .status(500)
          .json({ success: false, Message: "Database connection failure" });
      });
  }


module.exports = {
    editUsers
}