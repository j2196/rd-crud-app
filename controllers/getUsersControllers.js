const express = require("express");
const router = express.Router();
const fs = require("fs");
const path = require("path");
const collectionName = "usersData";
const connectionObject = require("../Db/connection");
const {adminEmail, pdfPassword, userName, userPassword } = require("../configurations")
let db;

connectionObject.initialDbConnection((error, dbInstance) => {
    if (error) {
      console.log(error);
      throw error;
    } else {
      db = dbInstance;
    }
  });

const getUsersController = (req, res) => {
    db.collection(collectionName)
      .find()
      .toArray()
      .then((data) => {
        res.render('pages/index', {
          data : data.filter((object) => !object.isDeleted)
        })
      })
      .catch((error) => {
        console.log(error);
        res
          .status(500)
          .json({ success: false, Message: "Database connection failure" });
      });
}

const createUserForm = (req, res) => {
    res.render('pages/createUserForm')
}

const verifyAccount = (req, res) => {
    let id = req.params.id;
    db.collection(collectionName).find().toArray()
    .then((data) => {
      data.map((object) => {
        if(object._id.toString() === id) {
          db.collection(collectionName).updateMany(
            { _id: object._id },
            {
                $set: {
                  status: 'Active'
              }
            })
            return object
        }
        return object
      })
    })
      .then(() => {
        res.redirect('/');
      })
      .catch((error) => {
        console.log(error);
        res
          .status(500)
          .json({ success: false, Message: "Database connection failure" });
      });
}

const editUserForm = (req, res) => {
    let id = req.params.id;
  
    db.collection(collectionName)
      .find()
      .toArray()
      .then((data) => {
        let requiredObject = data.filter((userObject) => {
            return userObject._id.toString() === id
        });
        return requiredObject
      })
      .then((data) => {
        res.render('pages/editUserForm', {
          data
        })
      })
      .catch((error) => {
        console.log(error);
        res
          .status(500)
          .json({ success: false, Message: "Database connection failure" });
      });
  }

const deleteUserForm = (req, res) => {
    let id = req.params.id;
    db.collection(collectionName).find().toArray()
      .then((data) => {
        return data.map((object) => {
          if(object._id.toString() === id) {
            db.collection(collectionName).updateMany(
              { _id: object._id },
              {
                  $set: {
                    isDeleted: true
                }
              }
              )
              return object
          }
          return object
        })
      })
      .then(() => {
        res.redirect('/');
      })
      .catch((error) => {
        console.log(error);
        res
          .status(500)
          .json({ success: false, Message: "Database connection failure" });
      });
    
  }

module.exports = {
    getUsersController,
    createUserForm,
    verifyAccount,
    editUserForm,
    deleteUserForm
}