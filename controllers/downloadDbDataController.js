const fs = require("fs");
const path = require("path");
const nodemailer = require("nodemailer");
const connectionObject = require("../Db/connection");
let db;
const collectionName = "usersData";

connectionObject.initialDbConnection((error, dbInstance) => {
  if(error) {
    console.log(error);
    throw error;
  } else {
    db = dbInstance;
  }
})
const {
  adminEmail,
  pdfPassword,
  userName,
  userPassword,
} = require("../configurations");

const pdfPasswordOptions = {
  userPassword: pdfPassword,
  margin: 30,
  size: "A4",
};

const plainPdfOptions = {
  margin: 30,
  size: "A4",
}

const PDFDocument = require("pdfkit-table");

function sendMailToUser(fileType) {
    //Senders Information
    const transporter = nodemailer.createTransport({
      service: "hotmail",
      auth: {
        user: userName,
        pass: userPassword,
      },
    });
  
    let pathOfFile = fileType.dataFormat.endsWith('PDF') ? path.join(__dirname,"../data.pdf") : path.join(__dirname,"../data.csv");
    //Receivers Info and mail body
    const options = {
      from: userName,
      to: `${adminEmail}`,
      subject: "Users Data",
      html: `<p>Password to open pdf "${pdfPassword}". Do not share your password with anyone.</p>`,
      attachments: [{
        path : pathOfFile
      }]
    };
  
    //Sending Email
    transporter.sendMail(options, (error, info) => {
      if (error) {
        console.log(error);
      } else {
        console.log("Email Sent: ", info.response);
      }
    });
  }

  //creating pdf based on request
const createPdfDocument = (data, pdfOptions) => {
  let doc = new PDFDocument(pdfOptions);

  doc.pipe(fs.createWriteStream("./data.pdf"));
  let requiredData = data.map((object) => {
    return {
      "name": object.name,
      "email": object.email,
      "status": object.status
    }
  })
  const tableJson = { 
    "headers": [
      { "label":"Name", "property":"name"},
      { "label":"Email", "property":"email"},
      { "label":"Status", "property":"status"}
    ],
    "datas": requiredData
  }
  doc.table(tableJson); 
  doc.end();

}  
  
const userDataFromDb = (req, res) => {
    if(req.body.dataFormat.endsWith('PDF')) {
      db.collection(collectionName)
      .find()
      .toArray()
      .then((data) => {
        if(req.body.dataFormat.startsWith('Data')) {
          createPdfDocument(data, pdfPasswordOptions)
        } else {
          createPdfDocument(data, plainPdfOptions)
        }
      })
      //first making pdf then based on the request formatting the response
      .then(() => {
        if(req.body.dataFormat.startsWith('Data')) {
          sendMailToUser(req.body)
          res.render('pages/downloadUserData')
        } else {
          res.download(path.join(__dirname, "../data.pdf"))
        }
      })
      .catch((error) => {
        console.log(error);
      });
    } else {
      db.collection(collectionName)
      .find()
      .toArray()
      .then((data) => {
        fs.writeFile("./data.csv", JSON.stringify(data), "utf8", (error) => {
          if (error) {
            console.log(error);
          } else {
            console.log('Data Written Successfully in csv format')
          }
        });
      })
      .then(() => {
        if(req.body.dataFormat.startsWith('Data')) {
          sendMailToUser(req.body)
          res.render('pages/downloadUserData')
        } else {
          res.download(path.join(__dirname, "../data.csv"))
        }
      })
      .catch((error) => {
        console.log(error);
      }); 
    }
    
  }
  

module.exports = {
  userDataFromDb,
};
