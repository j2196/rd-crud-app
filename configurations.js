const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  port: process.env.PORT,
  url: process.env.DATABASE_URL,
  dbName: process.env.DATABASE_NAME,
  userName: process.env.USER_EMAIL,
  userPassword: process.env.USER_PASSWORD,
  adminEmail: process.env.ADMIN_EMAIL,
  pdfPassword: process.env.PDF_PASSWORD
};